FROM openjdk:8-jdk-alpine
EXPOSE 4003
VOLUME /main-app
ADD /target/eureka-service-0.1.0.jar eureka-service-0.1.0.jar
ENTRYPOINT ["java","-jar","eureka-service-0.1.0.jar"]